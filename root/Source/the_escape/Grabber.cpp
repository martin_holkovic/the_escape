// Copyright @ bo100nka

#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	GetPhysicsHandleComponent();
	GetPlayerInputComponent();
}

void UGrabber::GetPhysicsHandleComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (PhysicsHandle != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s: PhysicsHandle!"), *GetOwner()->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Unable to find PhysicsHandle!"));
	}
}

void UGrabber::GetPlayerInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Unable to find the InputComponent!"));
	}
	else
	{
		InputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Release);
	}
}

void UGrabber::Grab()
{
	if (PhysicsHandle == nullptr) return;

	FHitResult hit = GetFirstPhysicsBodyInReach();

	if (hit.GetActor() != nullptr)
	{
		UPrimitiveComponent* compFound = hit.GetComponent();

		if (compFound != nullptr)
			PhysicsHandle->GrabComponent(compFound, EName::NAME_None, compFound->GetOwner()->GetActorLocation(), true);
	}
}

void UGrabber::Release()
{
	if (PhysicsHandle != nullptr)
		PhysicsHandle->ReleaseComponent();
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	// Try and reach any actors with the PhysicsBody collision channel set
	FHitResult hitResult;
	FCollisionQueryParams params = FCollisionQueryParams(FName(TEXT("")), false, 0);
	bool bHit = GetWorld()->LineTraceSingleByObjectType(OUT hitResult, GetPlayerLineTraceStart(), GetPlayerLineTraceEnd(), FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), params);

	// if we hit something, we should attach the PhysicsHandle component
	if (bHit)
	{
		AActor* actorHit = hitResult.GetActor();

		if (actorHit)
		{
			UE_LOG(LogTemp, Warning, TEXT("Target: %s"), *actorHit->GetName());
		}
	}

	return hitResult;
}

void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// move the object we are holding
	if (PhysicsHandle != nullptr && PhysicsHandle->GrabbedComponent != nullptr)
	{
		PhysicsHandle->SetTargetLocation(GetPlayerLineTraceEnd());
	}
}

FVector UGrabber::GetPlayerLineTraceStart() const
{
	/// get location and rotation
	FVector VPlayerViewPointLocation;
	FRotator RPlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT VPlayerViewPointLocation,
		OUT RPlayerViewPointRotation
	);

	return VPlayerViewPointLocation;
}

FVector UGrabber::GetPlayerLineTraceEnd() const
{
	/// get location and rotation
	FVector VPlayerViewPointLocation;
	FRotator RPlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT VPlayerViewPointLocation,
		OUT RPlayerViewPointRotation
	);

	return GetPlayerLineTraceStart() + RPlayerViewPointRotation.Vector() * Reach;
}