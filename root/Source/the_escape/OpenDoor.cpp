// Copyright @ bo100nka

#include "OpenDoor.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	if (PressurePlate == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s is missing PressurePlate reference!"), *GetOwner()->GetName());
	}
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActorsOnPressurePlate() > PressureNeeded)
		OpenDoorRequested.Broadcast();
	else
		CloseDoorRequested.Broadcast();
}

float UOpenDoor::GetTotalMassOfActorsOnPressurePlate() const
{
	if (PressurePlate == nullptr) return 0.f;

	TArray<AActor*> OverlappingActors;
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);
	
	if (OverlappingActors.Num() == 0) return 0.f;

	float massTotal = 0.f;
	for (const AActor* actorItem : OverlappingActors)
	{
		massTotal += actorItem->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		///UE_LOG(LogTemp, Warning, TEXT("Mass: %f"), massTotal); /// debug purpose
	}
	return massTotal;
}

