// Copyright @ bo100nka

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Public/Delegates/Delegate.h"
#include "OpenDoor.generated.h"


// declare new event class for Open door request
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUseDoorRequest);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	

	// Event delegate that is fired once receiving the request to open the door
	UPROPERTY(BlueprintAssignable)
	FUseDoorRequest OpenDoorRequested;

	// Event delegate that is fired once receiving the request to close the door
	UPROPERTY(BlueprintAssignable)
	FUseDoorRequest CloseDoorRequested;

	// Sets default values for this component's properties
	UOpenDoor();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


private:

	// Returns the total mass [kg] of actors that overlap the associated pressure plate that should open this door
	float GetTotalMassOfActorsOnPressurePlate() const;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate = nullptr;
	
	UPROPERTY(EditAnywhere)
	float PressureNeeded = 20.0f;
};
