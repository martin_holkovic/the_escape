// Copyright @ bo100nka

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	// How far can the owner reach for tracing an object
	float Reach = 100.f;

	// reference to player's (pawns) input component dynamically attached after game started
	UInputComponent* InputComponent = nullptr;
	
	// reference to player's (pawns) component that can manipulate physics with other objects surrounded by
	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	// gets the reference of the PhysicsHandle component that should be attached on the pawn
	void GetPhysicsHandleComponent();

	// gets the reference of the input component that should be attached on the pawn
	void GetPlayerInputComponent();

	// reaches out in front of pawn for actors with the physical body component and enters the grab mode
	void Grab();

	// drops the physical actor held by the Grab method
	void Release();
	
	FHitResult GetFirstPhysicsBodyInReach() const;

	FVector GetPlayerLineTraceStart() const;

	FVector GetPlayerLineTraceEnd() const;
};
